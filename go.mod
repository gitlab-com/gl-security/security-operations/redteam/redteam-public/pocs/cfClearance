module gitlab.com/gitlab-com/gl-security/threatmanagement/redteam/redteam-public/cfClearance

go 1.14

require (
	github.com/chromedp/cdproto v0.0.0-20200709115526-d1f6fc58448b
	github.com/chromedp/chromedp v0.5.3
)
